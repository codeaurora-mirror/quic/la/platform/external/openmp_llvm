LOCAL_PATH := $(call my-dir)

libomp_src_files := \
    runtime/src/kmp_affinity.cpp \
    runtime/src/kmp_alloc.cpp \
    runtime/src/kmp_atomic.cpp \
    runtime/src/kmp_barrier.cpp \
    runtime/src/kmp_cancel.cpp \
    runtime/src/kmp_csupport.cpp \
    runtime/src/kmp_debug.cpp \
    runtime/src/kmp_dispatch.cpp \
    runtime/src/kmp_environment.cpp \
    runtime/src/kmp_error.cpp \
    runtime/src/kmp_ftn_cdecl.cpp \
    runtime/src/kmp_ftn_extra.cpp \
    runtime/src/kmp_global.cpp \
    runtime/src/kmp_gsupport.cpp \
    runtime/src/kmp_i18n.cpp \
    runtime/src/kmp_io.cpp \
    runtime/src/kmp_itt.cpp \
    runtime/src/kmp_lock.cpp \
    runtime/src/kmp_runtime.cpp \
    runtime/src/kmp_sched.cpp \
    runtime/src/kmp_settings.cpp \
    runtime/src/kmp_str.cpp \
    runtime/src/kmp_taskdeps.cpp \
    runtime/src/kmp_tasking.cpp \
    runtime/src/kmp_taskq.cpp \
    runtime/src/kmp_threadprivate.cpp \
    runtime/src/kmp_utility.cpp \
    runtime/src/kmp_version.cpp \
    runtime/src/kmp_wait_release.cpp \
    runtime/src/thirdparty/ittnotify/ittnotify_static.c \
    runtime/src/z_Linux_asm.s \
    runtime/src/z_Linux_util.cpp \

libomp_c_includes := $(LOCAL_PATH)/runtime/src \
                     $(LOCAL_PATH)/runtime/src/generated/ \
                     $(LOCAL_PATH)/runtime/src/thirdparty/ittnotify

# --undefined-version is needed because exports_so.txt references symbols that
#  are not defined by default.
libomp_ldflags := -Wl,--warn-shared-textrel \
                  -Wl,--as-needed \
                  -Wl,-fini=__kmp_internal_end_fini \
                  -Wl,-x \
                  -Wl,--version-script,$(LOCAL_PATH)/runtime/src/exports_so.txt \
                  -Wl,--undefined-version \
                  -Wl,-z,noexecstack

libomp_cppflags := -std=c++11 -Wno-unused-parameter

# z_Linux_asm.s needs to be pre-processed
libomp_asflags := -x assembler-with-cpp

libomp_ldlibs := -ldl

# Target: for arm, arm64, x86, x86_64 only
# =====================================================

include $(CLEAR_VARS)

LOCAL_MODULE := libomp-$(TARGET_ARCH)

LOCAL_SRC_FILES := $(libomp_src_files)

# We don't have message catalog APIs on Android until O. For releases prior to
# that, we build in stub APIs (as weak symbols so we use libc's on new API
# levels). We add these here instead of in libandroid_support because the driver
# (correctly) links libomp well after libandroid_support, which means
# libandroid_support won't be searched for resolving symbols in openmp.
LOCAL_SRC_FILES += runtime/src/android_nltypes_stubs.cpp

LOCAL_LDFLAGS := $(libomp_ldflags)
LOCAL_CPPFLAGS := $(libomp_cppflags)
LOCAL_ASFLAGS := $(libomp_asflags)

# Include target-specific generated headers
LOCAL_C_INCLUDES := $(libomp_c_includes) \
                    $(LOCAL_PATH)/runtime/src/generated/$(TARGET_ARCH)

# 32-bit prebuilts get generated as a part of their own target
LOCAL_MULTILIB := first

LOCAL_LDLIBS := $(libomp_ldlibs)

LOCAL_SDK_VERSION := 14
LOCAL_NDK_STL_VARIANT := c++_static

LOCAL_MODULE_TAGS := optional

include $(BUILD_STATIC_LIBRARY)

# For the host, so we can run the upstream lit tests
# =====================================================

include $(CLEAR_VARS)

LOCAL_MODULE := libomp

LOCAL_SRC_FILES := $(libomp_src_files)
LOCAL_LDFLAGS := $(libomp_ldflags)
LOCAL_CPPFLAGS := $(libomp_cppflags)
LOCAL_ASFLAGS := $(libomp_asflags)

# Include generated headers for HOST_ARCH
LOCAL_C_INCLUDES := $(libomp_c_includes) \
                    $(LOCAL_PATH)/runtime/src/generated/$(HOST_ARCH)

LOCAL_LDLIBS := $(libomp_ldlibs) -lpthread

LOCAL_CXX_STL := libc++_static

# Host tests only need x86_64
LOCAL_MULTILIB := first

LOCAL_MODULE_TAGS := optional

include $(BUILD_HOST_SHARED_LIBRARY)
