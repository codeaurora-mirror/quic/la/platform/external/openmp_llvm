#!/usr/bin/env python
#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from __future__ import print_function

import os
import shutil
import subprocess


THIS_DIR = os.path.realpath(os.path.dirname(__file__))
ORIG_ENV = dict(os.environ)


def android_path(*args):
    return os.path.realpath(os.path.join(THIS_DIR, '../..', *args))


def out_root():
    return android_path('out', 'openmp-cmake')


def cmake_out(arch):
    return os.path.join(out_root(), arch)


def cmake_cmd(arch):
    # OpenMP expects LIBOMP_ARCH to be i386 for x86 and aarch64 for arm64
    if arch == 'x86':
        arch = 'i386'
    elif arch == 'arm64':
        arch = 'aarch64'
    return (
        'cmake',
        THIS_DIR,
        '-DLIBOMP_ARCH=%s' % arch,

        # Can't use versioned symbols with static libraries.
        '-DLIBOMP_USE_VERSION_SYMBOLS=OFF',
    )


# Run cmake configuration step for this particular arch
def invoke_cmake_config(arch):
    config_dir = cmake_out(arch)

    # Blow away config_dir, if it exists
    if os.path.isdir(config_dir):
        shutil.rmtree(config_dir)
    os.makedirs(config_dir)

    # TODO We are using system gcc for configuring below.  Right now, there is
    # no difference between using clang or gcc as the compiler during cmake
    # configuration.  Might need to revisit in the future.
    subprocess.check_call(cmake_cmd(arch), cwd=config_dir)


# Copy the two header files generated during cmake configuration to their
# architecture-specific directories
def copy_headers(arch):
    config_dir = cmake_out(arch)
    dst_dir = os.path.join(THIS_DIR, 'runtime/src/generated', arch)
    if not os.path.isdir(dst_dir):
        os.makedirs(dst_dir)

    files = ['kmp_config.h', 'omp.h']
    for fname in files:
        src = os.path.join(config_dir, 'runtime/src', fname)
        dst = os.path.join(dst_dir, fname)
        print('Copying ' + src)
        shutil.copy2(src, dst)


# Create i18n header for this particular output-kind and the corresponding CLI
# arg
def create_i18n_header(output, arg):
    script = os.path.join(THIS_DIR, 'runtime/tools/message-converter.pl')
    en_US = os.path.join(THIS_DIR, 'runtime/src/i18n/en_US.txt')

    command = ['perl', script, '--os=lin', '--prefix=kmp_i18n',
               '--{}=kmp_i18n_{}.inc'.format(arg, output), en_US]
    subprocess.check_call(command, cwd=out_root())

# Create the two i18n headers used in OpenMP and copy them to the desired output
# directory
def generate_i18n_headers():
    output_to_arg = {'id': 'enum',
                     'default': 'default'}
    for output in output_to_arg:
        create_i18n_header(output, output_to_arg[output])

        src = os.path.join(out_root(), 'kmp_i18n_{}.inc'.format(output))
        dst = os.path.join(THIS_DIR, 'runtime/src/generated')
        shutil.copy2(src, dst)

def main():
    # Invoke cmake configuration and copy the generated headers
    for arch in ['arm', 'arm64', 'mips', 'mips64', 'x86', 'x86_64']:
        invoke_cmake_config(arch)
        copy_headers(arch)

    # Generate and copy i18n headers
    generate_i18n_headers()

if __name__ == '__main__':
    main()
